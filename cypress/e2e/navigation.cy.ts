
describe('Navigation Tests', () => {
// NAV RENDERS
  it('Should appear on desktop viewports', () => {
    cy.viewport(1300, 800);
    cy.visit('http://localhost:3000/')
    cy.wait(3000);
    cy.get('nav').should('exist')
  })

  it('Should appear on tablet viewports', () => {
    cy.viewport(800, 800);
    cy.visit('http://localhost:3000/')
    cy.wait(3000);
    cy.get('nav').should('exist')
  })

  it('Should appear on mobile viewports', () => {
    cy.viewport(500, 800);
    cy.visit('http://localhost:3000/')
    cy.wait(3000);
    cy.get('nav').should('exist')
  })

// NAV LINKS
  it('Should test all level one and two navigation links for 404 errors on desktop', () => {
    cy.viewport(1300, 800);
    cy.visit('http://localhost:3000/'); // Replace with your URL
  
    cy.get('#be-navigation-desktop').within(() => {
      cy.get('a').each(($link) => {
        const href = $link.attr('href'); 
        
  
        // Cloudflare blocks bots from clicking on signup/signin
        if (!/(sign_in|sign_up)/.test(href)) {
          cy.request(href).its('status').should('not.eq', 404);
        }
    });
  });
})

it('Should test all level one and two navigation links for 404 errors on mobile', () => {
  cy.viewport(700, 800);
  cy.visit('http://localhost:3000/'); // Replace with your URL

  cy.get('#be-navigation-mobile').within(() => {
    cy.get('a').each(($link) => {
      const href = $link.attr('href'); 
      

      // Cloudflare blocks bots from clicking on signup/signin
      if (!/(sign_in|sign_up)/.test(href)) {
        cy.request(href).its('status').should('not.eq', 404);
      }
  });
});
})

// SEARCH 
it('Should have a working search functionality on desktop', () => {
  cy.viewport(1300, 800)
  cy.visit('http://localhost:3000')

  cy.get('button[data-nav="site search"]').click()
  cy.get('.be-nav-search-content__input input').type('Testing')
  cy.get('.be-nav-suggestions .be-nav-suggestions__result').should('be.greaterThan', 2)
  cy.get('.be-nav-search-content__input input').type({enter})
  cy.url().contains('eq', 'https://about.gitlab.com/search?searchText=testing')
})


// FOOTER LINKS
  it('Should test all footer links for 404 errors', () => {
    cy.viewport(1300, 800);
    cy.visit('http://localhost:3000/'); // Replace with your URL
  
    cy.get('.footer__container').within(() => {
      cy.get('a').each(($link) => {
        const href = $link.attr('href'); 
        // Twitter link fails because it redirects too many times
        if (!/(twitter)/.test(href)) {
          cy.request(href).its('status').should('not.eq', 404);
        }
    });
  });
})







})