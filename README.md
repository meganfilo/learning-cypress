## Set up instructions

1. `yarn add -D cypress`


2. Create `cypress.config.ts` in root of project

```
import { defineConfig } from "cypress";

export default defineConfig({
  e2e: {
    baseUrl: BASE_URL,
    setupNodeEvents(on, config) {
      on('task', {
        log(message) {
          console.log(message)

          return null
        },
      })
    },
  },
});

```

